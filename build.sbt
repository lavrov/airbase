name := "airbase"

version in ThisBuild := "0.1"

scalaVersion in ThisBuild := "2.12.3"

lazy val repository = (
  project in file("modules/repository")
  settings (
    libraryDependencies ++= Seq(
      "com.github.tototoshi" %% "scala-csv" % "1.3.5"
    )
  )
)

lazy val server = (
  project in file("modules/server")
  dependsOn repository
  settings {
    val browserifyTask = taskKey[Seq[File]]("Run browserify")
    val browserifyOutputDir = settingKey[File]("Browserify output directory")

    Seq(
      libraryDependencies ++= Seq(
        guice,
        "org.webjars.npm" %	"react-router" % "3.0.0"
      ),

      browserifyOutputDir := target.value / "web" / "browserify",

      browserifyTask := {
        println("Running browserify")
        val outputFile = browserifyOutputDir.value / "main.js"
        browserifyOutputDir.value.mkdirs
        val stderrBuffer = new scala.collection.mutable.ListBuffer[String]
        val cmd = new java.lang.ProcessBuilder(
          "./node_modules/.bin/browserify",
          "-t",
          "[", "babelify", "--presets", "[", "es2015", "react", "]", "]",
          "app/assets/javascripts/main.jsx",
          "-o", outputFile.getPath
        ).directory(file("./modules/server"))
        val status = cmd ! new ProcessLogger {
          def info(s: => String) {}

          def error(s: => String) {
            stderrBuffer.append(s)
          }

          def buffer[T](f: => T): T = f
        }
        if (status != 0) sys.error(stderrBuffer.mkString("\n", "\n", "\n"))
        List(outputFile)
      },

      sourceGenerators in Assets += browserifyTask,

      resourceDirectories in Assets += browserifyOutputDir.value
    )
  }
  enablePlugins (PlayScala, SbtWeb)
)

libraryDependencies in ThisBuild += "org.scalatest" %% "scalatest" % "3.0.1" % Test
