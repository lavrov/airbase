package controllers

import javax.inject.Inject

import play.api.libs.json.{JsString, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import com.github.lavrov.airbase.repository.{AirportRepository, CountryRepository, RunwayRepository}

import scala.concurrent.ExecutionContext

class QueryController @Inject()(
    countryRepository: CountryRepository,
    airportRepository: AirportRepository,
    runwayRepository: RunwayRepository,
    cc: ControllerComponents)(
    implicit
      ec: ExecutionContext)
  extends AbstractController(cc) {
  import Formats._

  def countries(filterTerm: Option[String]) = Action {
    val result =
      filterTerm match {
        case Some(term) =>
          countryRepository.search(term)
        case None =>
          countryRepository.getAll
      }

    Ok(Json toJson result)
  }

  def country(code: String) = Action {
    countryRepository.getByCode(code) match {
      case Some(country) =>
        Ok(Json toJson country)
      case None =>
        NotFound(JsString(s"Country with code $code not found"))
    }
  }

  def airport(ident: String) = Action {
    airportRepository.getByIdent(ident) match {
      case Some(airpot) =>
        Ok(Json toJson airpot)
      case None =>
        NotFound(JsString(s"Airports with ident $ident not found"))
    }
  }

  def countryAirports(countryCode: String) = Action {
    val airports = airportRepository.findByCountryCode(countryCode)
    Ok(Json toJson airports)
  }

  def airportRunways(airportIdent: String) = Action {
    val runways = runwayRepository.findByAirportIdent(airportIdent)
    Ok(Json toJson runways)
  }
}
