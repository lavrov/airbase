package controllers

import com.github.lavrov.airbase.repository.ReportApi.{CountryNumberOfAirports, CountrySurfaces}
import com.github.lavrov.airbase.{Airport, Country, Runway}
import play.api.libs.json.Json

object Formats {

  implicit val countryWriter = Json.writes[Country]

  implicit val airportWriter = Json.writes[Airport]

  implicit val runwayWriter = Json.writes[Runway]

  implicit val countryNumberOfAirportsWriter = Json.writes[CountryNumberOfAirports]

  implicit val countrySurfacesWriter = Json.writes[CountrySurfaces]

}
