package controllers

import javax.inject.Inject

import com.github.lavrov.airbase.repository.ReportApi
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}

class ReportController @Inject()(
    cc: ControllerComponents,
    reportApi: ReportApi
) extends AbstractController(cc) {
  import Formats._

  def topCountries(ascending: Boolean) = Action {
    val result = reportApi.topCountries(ascending)
    Ok(Json toJson result)
  }

  def surfacePerCountry = Action {
    val result = reportApi.surfacePerCountry
    Ok(Json toJson result)
  }

  def runwayCommonLeIdent = Action {
    val result = reportApi.topCommonRunwayIdentifications
    Ok(Json toJson result)
  }
}
