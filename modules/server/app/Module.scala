import com.github.lavrov.airbase.{Airport, Country, Runway}
import com.github.lavrov.airbase.csv.CsvReader
import play.api.inject.Binding
import play.api.{Configuration, Environment}
import com.github.lavrov.airbase.repository._

class Module extends play.api.inject.Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = {
    val basePath = configuration.get[String]("source.directory")
    val database = new SimpleDatabase(
      countries = CsvReader.fromFile[Country](s"$basePath/countries.csv").toList,
      airports = CsvReader.fromFile[Airport](s"$basePath/airports.csv").toList,
      runways = CsvReader.fromFile[Runway](s"$basePath/runways.csv").toList,
    )

    Seq(
      bind[CountryRepository] toInstance new SimpleCountryRepository(database),

      bind[AirportRepository] toInstance new SimpleAirportRepository(database),

      bind[RunwayRepository] toInstance new SimpleRunwayRepository(database),

      bind[ReportApi] toInstance new SimpleReportApi(database),
    )
  }
}
