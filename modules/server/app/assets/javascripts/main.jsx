import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Redirect, hashHistory} from 'react-router'
import Countries from './countries.jsx';
import Airports from './airports.jsx';
import Runways from './runways.jsx';
import {TopCountries, CountrySurface, MostCommonLeIdent} from './report.jsx';
import {Link} from 'react-router';

var routes =
    <div>
        <a href="#">Home</a>
        <hr/>
        <Router history={hashHistory}>
            <Route path="/" component={App}/>
            <Route path="/countries" component={Countries}/>
            <Route path="/countries/:country/airports" component={Airports}/>
            <Route path="/countries/:country/airports/:airport/runways" component={Runways}/>
            <Route path="/report/country/top" component={TopCountries}/>
            <Route path="/report/country/surface" component={CountrySurface}/>
            <Route path="/report/country/common-le-ident" component={MostCommonLeIdent}/>
            <Redirect from="/*" to="/"/>
        </Router>
    </div>;

ReactDOM.render(routes, document.getElementById('app'));


function App(props) {
    return (
        <div>
            <h4>Welcome</h4>
            <ul>
                <li><Link to="/countries">Query</Link></li>
                <li>Reports</li>
                <ul>
                    <li><Link to="/report/country/top">Top countries</Link></li>
                    <li><Link to="/report/country/surface">Country runway surfaces</Link></li>
                    <li><Link to="/report/runway/common-le-ident">Most common le_ident</Link></li>
                </ul>
            </ul>
        </div>
    )
}
