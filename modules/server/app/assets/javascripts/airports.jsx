import React from 'react';
import axios from 'axios';
import {Link} from 'react-router';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
        this.state = {airports: [], country: props.params.country};
    }

    componentDidMount() {
        axios.get('/countries/' + this.state.country + "/airports")
            .then(({data}) => this.setState({airports: data}));
    }

    render() {
        var airports = this.state.airports.map((airport) =>
            <tr key={airport.ident}>
                <td>
                    <Link to={"/countries/" + this.state.country + "/airports/" + airport.ident + "/runways"}>
                        {airport.ident}
                    </Link>
                </td>
                <td>
                    {airport.name}
                </td>
                <td>
                    {airport.iata}
                </td>
            </tr>
        );
        return (
            <div>
                <p>Country {this.state.country}</p>
                <table>
                    <thead>
                    <tr>
                        <th>ident</th>
                        <th>name</th>
                        <th>iata</th>
                    </tr>
                    </thead>
                    <tbody>
                        {airports}
                    </tbody>
                </table>
            </div>
        )

    }
}
