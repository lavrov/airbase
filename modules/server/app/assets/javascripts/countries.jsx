import React from 'react';
import {Link} from 'react-router';
import axios from 'axios';

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {term: '', countries: []};

        this.termChanged = this.termChanged.bind(this);
    }

    fetchCountries(term) {
        var params = term ? {filter: term} : null;
        axios.get('/countries', {params: params})
            .then(({data}) => this.setState({countries: data}))
    }

    termChanged(e) {
        var term = e.target.value;
        this.setState({term: term});
        this.fetchCountries(term);
    }

    componentDidMount() {
        this.fetchCountries();
    }

    render() {
        return (
            <div>
                <label htmlFor="term">Country code or name</label><br/>
                <input type="text" id="term" value={this.state.term}
                       onChange={this.termChanged}/>
                <CountryList countries={this.state.countries}/>
            </div>
        )
    }
}

function CountryList(props) {
    var items = props.countries.map((c) => <CountryItem country={c} key={c.code}/>);
    return <ul>{items}</ul>
}

function CountryItem(props) {
    return (
        <li>
            <Link to={"/countries/" + props.country.code + "/airports"}>
                <span>{props.country.code}</span> <span>{props.country.name}</span> <span>({props.country.continent})</span>
            </Link>
        </li>
    );
}
