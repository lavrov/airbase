import React from 'react';
import axios from 'axios';
import {Link} from 'react-router';

export class TopCountries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {top: [], bottom: []};
    }

    componentDidMount() {
        axios.get("/report/country/top", {params: {ascending: false}})
            .then(({data}) => this.setState({top: data}));
        axios.get("/report/country/top", {params: {ascending: true}})
            .then(({data}) => this.setState({bottom: data}));
    }

    render() {
        var top = this.state.top.map(({country, numberOfAirports}) =>
            <li key={country.ident}>
                {country.name}: {numberOfAirports}
            </li>
        );
        var bottom = this.state.bottom.map(({country, numberOfAirports}) =>
            <li key={country.code}>
                {country.name}: {numberOfAirports}
            </li>
        );
        return (
            <div>
                <p>Countries with highest number of airports</p>
                <ul>{top}</ul>
                <p>Countries with lowest number of airports</p>
                <ul>{bottom}</ul>
            </div>
        )
    }
}


export class CountrySurface extends React.Component {
    constructor(props) {
        super(props);
        this.state = {report: []};
    }

    componentDidMount() {
        axios.get("/report/country/surfaces")
            .then(({data}) => this.setState({report: data}));
    }

    render() {
        var report = this.state.report.map(({country, surfaces}) => {
            var surfaceList = surfaces.map((s, i) => <li key={i}>{s}</li>)
            return (
                <li key={country.code}>
                    {country.name}
                    <ul>{surfaceList}</ul>
                </li>
            )
        });
        return (
            <div>
                <p>Type of runways per country</p>
                <ul>{report}</ul>
            </div>
        )
    }
}
