import React from 'react';
import axios from 'axios';
import {Link} from 'react-router';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
        this.state = {runways: [], country: props.params.country, airport: props.params.airport};
    }

    componentDidMount() {
        axios.get("/airports/" + this.state.airport + "/runways")
            .then(({data}) => this.setState({runways: data}));
    }

    render() {
        var runways = this.state.runways.map((runway) =>
            <tr key={runway.id}>
                <td>{runway.id}</td>
                <td>{runway.surface}</td>
                <td>{runway.length}</td>
            </tr>
        );
        return (
            <div>
                <p>Airport {this.state.airport}</p>
                <table>
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>surface</th>
                        <th>length</th>
                    </tr>
                    </thead>
                    <tbody>
                    {runways}
                    </tbody>
                </table>
            </div>
        )
    }
}

