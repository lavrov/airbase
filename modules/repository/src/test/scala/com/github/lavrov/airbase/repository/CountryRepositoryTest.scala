package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.Country
import com.github.lavrov.airbase.csv.CsvReader
import org.scalatest.{FlatSpec, Inside}
import org.scalatest.Matchers._

import scala.io.Source

class CountryRepositoryTest extends FlatSpec {

  val database = new SimpleDatabase(
    countries = CsvReader.fromSource[Country](Source fromURL getClass.getResource("/countries.csv")).toList
  )

  val repository: CountryRepository = new SimpleCountryRepository(database)

  behavior of "getByCode"

  it should "find country by code" in {
    Inside.inside(repository.getByCode("AO")){
      case Some(country) =>
        country.name shouldEqual "Angola"
    }
    repository.getByCode("BB") shouldBe 'empty
  }

  behavior of "getAll"

  it should "return all countries" in {
    repository.getAll should have size 9
  }

  behavior of "search"

  it should "find country by name" in {
    val result = repository.search("An")
    result should have size 4
    result(0).name shouldEqual "Andorra"
    result(1).name shouldEqual "Antigua and Barbuda"
    result(2).name shouldEqual "Anguilla"
    result(3).name shouldEqual "Angola"
  }

  it should "find country by code" in {
    val result = repository.search("AI")
    result should have size 1
    result.head.name shouldEqual "Anguilla"
  }

  it should "find country by code and name" in {
    val result = repository.search("AR")
    result should have size 3
    result(0).name shouldEqual "Artificial country"
    result(1).name shouldEqual "United Arab Emirates"
    result(2).name shouldEqual "Armenia"
  }

}
