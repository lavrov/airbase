package com.github.lavrov.airbase.csv

import com.github.lavrov.airbase.{Airport, Country, Runway}
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

import scala.io.Source

class CsvReaderTest extends FlatSpec {

  behavior of "CsvReader"

  it should "convert csv lines into countries" in {
    val list = CsvReader.fromSource[Country](Source.fromURL(getResource("/countries.csv"))).toList
    list should have size 9
    val first = list.head
    first.name shouldEqual "Andorra"
  }

  it should "convert csv lines into airports" in {
    val list = CsvReader.fromSource[Airport](Source.fromURL(getResource("/airports.csv"))).toList
    list should have size(4)
    val first = list.head
    first.id shouldEqual 6523L
    first.countryCode shouldEqual "US"
  }

  it should "convert csv lines into runways" in {
    val list = CsvReader.fromSource[Runway](Source.fromURL(getResource("/runways.csv"))).toList
    list should have size(9)
    val first = list.head
    first.id shouldEqual 269408L
    first.airportIdent shouldEqual "00A"
  }

  def getResource(path: String) = getClass.getResource(path)

}
