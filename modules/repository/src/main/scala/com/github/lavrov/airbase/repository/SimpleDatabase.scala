package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.{Airport, Country, Runway}

case class SimpleDatabase(
    countries: List[Country] = Nil,
    airports: List[Airport] = Nil,
    runways: List[Runway] = Nil) {

  val countryCodeIndex = countries.view.map(c => (c.code, c)).toMap

  val airportsByCountryIndex = airports.groupBy(_.countryCode)

  val runwaysByAirportIdentIndex = runways.groupBy(_.airportIdent)
}
