package com.github.lavrov.airbase.csv

import com.github.tototoshi.csv.CSVReader

import scala.io.Source

object CsvReader {

  def fromSource[A](source: Source)(implicit reader: LineConverter[A]): Iterator[A] = {
    val csvIterator = CSVReader.open(source).iteratorWithHeaders
    csvIterator.map(reader.read)
  }

  def fromFile[A: LineConverter](path: String) = fromSource[A](Source fromFile path)

}
