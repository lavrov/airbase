package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.Country

trait CountryRepository {

  def getAll: List[Country]

  def search(term: String): List[Country]

  def getByCode(code: String): Option[Country]
}

class SimpleCountryRepository(database: SimpleDatabase) extends CountryRepository {
  import database.{countries, countryCodeIndex}

  val nameIndex = countries.map(c => (c.name.toLowerCase.split(' '), c))

  def getAll: List[Country] = countries

  def search(term: String): List[Country] = {
    val lcTerm = term.toLowerCase
    val byCodeResult = countryCodeIndex.get(term)
    val byNameResult = nameIndex.collect {
      case (terms, c) if terms.exists(_ startsWith lcTerm) && c.code != term => c
    }
    byCodeResult.toList ++ byNameResult
  }

  def getByCode(code: String): Option[Country] = countryCodeIndex.get(code)
}
