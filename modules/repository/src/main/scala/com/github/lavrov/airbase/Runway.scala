package com.github.lavrov.airbase

case class Runway(
                   id: Long,
                   airportIdent: String,
                   surface: String,
                   leIdent: String,
                   length: String)
