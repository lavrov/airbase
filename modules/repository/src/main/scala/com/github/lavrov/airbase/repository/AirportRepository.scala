package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.Airport

trait AirportRepository {
  def getByIdent(code: String): Option[Airport]

  def findByCountryCode(countryCode: String): List[Airport]
}

class SimpleAirportRepository(database: SimpleDatabase) extends AirportRepository {
  import database.{airports, airportsByCountryIndex}

  val byIdentIndex = airports.view.map(a => (a.ident, a)).toMap

  def getByIdent(ident: String) = byIdentIndex.get(ident)

  def findByCountryCode(countryCode: String) = airportsByCountryIndex.get(countryCode) getOrElse Nil
}
