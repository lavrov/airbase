package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.Country
import com.github.lavrov.airbase.repository.ReportApi.{CountryNumberOfAirports, CountrySurfaces}

trait ReportApi {

  def topCountries(ascending: Boolean): List[CountryNumberOfAirports]

  def surfacePerCountry: List[CountrySurfaces]

  def topCommonRunwayIdentifications: List[String]
}

object ReportApi {

  case class CountryNumberOfAirports(country: Country, numberOfAirports: Int)

  case class CountrySurfaces(country: Country, surfaces: List[String])
}

class SimpleReportApi(database: SimpleDatabase) extends ReportApi {
   private lazy val sortedAsc =
     database.countries.map {
       country =>
         val numberOfAirports = database.airportsByCountryIndex.get(country.code).fold(0)(_.size)
         CountryNumberOfAirports(country, numberOfAirports)
     }
     .sortBy(_.numberOfAirports)

  def topCountries(ascending: Boolean) =
    if (ascending)
      sortedAsc.take(10)
    else
      sortedAsc.view.reverse.take(10).toList

  def surfacePerCountry = {
    database.countries.map {
      country =>
        val surfaces =
          database.airportsByCountryIndex.get(country.code).fold(List.empty[String]) {
            airports =>
              val surfacesPerAirport =
                airports.map { airport =>
                  database.runwaysByAirportIdentIndex.get(airport.ident).fold(Set.empty[String])(_.map(_.surface).toSet)
                }
              surfacesPerAirport.foldLeft(Set.empty[String])(_ union _).toList
          }
        CountrySurfaces(country, surfaces)
    }
  }

  def topCommonRunwayIdentifications = {
    val identToCount =
      database.runways.groupBy(_.leIdent).mapValues(_.size).toList
    val sortedByCountDesc =
      identToCount.sortBy(_._2)(Ordering[Int].reverse)
    sortedByCountDesc.take(10).map(_._1)
  }
}
