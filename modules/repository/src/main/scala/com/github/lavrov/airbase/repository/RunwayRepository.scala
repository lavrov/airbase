package com.github.lavrov.airbase.repository

import com.github.lavrov.airbase.{Airport, Runway}

trait RunwayRepository {

  def findByAirportIdent(airportIdent: String): List[Runway]
}

class SimpleRunwayRepository(database: SimpleDatabase) extends RunwayRepository {

  def findByAirportIdent(airportIdent: String) =
    database.runwaysByAirportIdentIndex.get(airportIdent) getOrElse Nil
}
