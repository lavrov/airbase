package com.github.lavrov.airbase

case class Country(
    id: Long,
    code: String,
    name: String,
    continent: String,
    wikiLink: String)
