package com.github.lavrov.airbase.csv

import com.github.lavrov.airbase.{Airport, Country, Runway}

trait LineConverter[A] {
  def read(input: Map[String, String]): A
}

object LineConverter {

  implicit val country: LineConverter[Country] = { map =>
    Country(
      id = map("id").toLong,
      name = map("name"),
      code = map("code"),
      continent = map("continent"),
      wikiLink = map("wikipedia_link")
    )
  }

  implicit val airport: LineConverter[Airport] = { map =>
    Airport(
      id = map("id").toLong,
      ident = map("ident"),
      countryCode = map("iso_country"),
      name = map("name"),
      latitude = map("latitude_deg"),
      longitude = map("longitude_deg"),
      elevation = map("elevation_ft"),
      iata = map("iata_code")
    )
  }

  implicit val runway: LineConverter[Runway] = { map =>
    Runway(
      id = map("id").toLong,
      airportIdent = map("airport_ident"),
      surface = map("surface"),
      leIdent = map("le_ident"),
      length = map("length_ft")
    )
  }
}
