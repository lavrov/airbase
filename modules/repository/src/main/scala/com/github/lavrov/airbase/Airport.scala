package com.github.lavrov.airbase

case class Airport(
    id: Long,
    ident: String,
    countryCode: String,
    name: String,
    latitude: String,
    longitude: String,
    elevation: String,
    iata: String)
