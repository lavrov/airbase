## Prerequisites

- Java 8
- sbt
- node
- npm

## Run

Update `source.directory` in _applicaiton.conf_. That directory should containt all the csv files (countries.csv, airports.csv, runways.csv). See Makefile for the shortcuts.
